# Frontend Engineer Challenge

## Idea of the App 

The task is to implement a small webapp that will list the 50 most starred Javascript Github repos. 
You'll be fetching the sorted data directly from the Github GraphQL API (Github API explained down below). 

## Features

* As a User I should be able to list the 50 most starred Javascript Github repos. 
* As a User I should see the results as a list. One repository per row. 
* As a User I should see 10 results loaded in at a time. 
* As a User I should be able to keep scrolling and new results should appear (infinate loading).
* As a User I should be able to see for each repo/row the following details:
    * Repository name
    * Repository description 
    * Total number of stars for the repo. 
    * Total number of issues for the repo.
    * Username and avatar of the owner.

## Things to keep in mind 🚨

* Features are less important than code quality. Put more focus on code quality and less on speed and number of features implemented. 
* Your code will be evaluated based on: code structure, programming best practices, legibility (and not number of features implemented or speed). 
* The git commit history (and git commit messages) will also be evaluated.
* Please include an overview of your solution and the reasons for your decisions (patterns, libraries etc.) in the README.
* Please include instructions on how to run the project locally in the README.

## How to get the data from Github 

Github has a great GraphQL API. 

`https://docs.github.com/en/graphql/overview/about-the-graphql-api`

There is also a built in expoler to test your queries against.

`https://docs.github.com/en/graphql/overview/explorer`

Here is a starting point for a schema you can use. (This is incomplete)

```
{
  search(query: "language:JavaScript", type: REPOSITORY, first: 20) {
    repositoryCount
    edges {
      node {
        ... on Repository {
          name
          owner {
            ... on Organization {
              name
            }
          }
          openGraphImageUrl
          stargazers(orderBy: {field: STARRED_AT, direction: DESC}) {
            totalCount
          }
          updatedAt
        }
      }
    }
  }
}
```

## Mockups

![alt text](https://raw.githubusercontent.com/hiddenfounders/frontend-coding-challenge/master/mockup.png)

Here's what each element represents:

![alt text](https://raw.githubusercontent.com/hiddenfounders/frontend-coding-challenge/master/row_explained.png)

## Technologies to use 

Choose whatever front-end technology you're most familiar with. Below are some of the technologies our teams are familiar with:

* React
* Vue
* Angular
* You will also need the client side GraphQL library

## How to submit the challenge solution? 

Once you have finished your app and pushed it to your GitHub account, please follow the instructions below:

1. Set the Repo privacy to public. If you have any conserns with this let us know.
2. Add the repo link to Coderbyte.


